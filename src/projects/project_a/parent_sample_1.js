import React, { Component } from 'react';
import Collapses from './components/collapses';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import './project_a.scss';

class Project_A extends Component {
	state = {
		collapse_common: false,
		collapse_status: false,
		collapse_id: '',
		collapses: [
			{ id: 1, title: 'Sliding Banners', collapse_status: false, block_status: true },
			{ id: 2, title: 'Top Categories', collapse_status: false, block_status: true },
			{ id: 3, title: 'Image Grid', collapse_status: false, block_status: true },
			{ id: 4, title: 'Banner Images', collapse_status: false, block_status: true }
		],
		cards: [ { id: 1 }, { id: 2 }, { id: 3 } ]
	};

	expandCollapse = (id) => {
		const { collapse_id, collapses } = this.state;
		if (collapse_id == id) {
			collapses.find((element) => element.id == id).collapse_status = !collapses.find(
				(element) => element.id == id
			).collapse_status;
		} else {
			collapses.find((element) => element.id == id).collapse_status = true;
			if (collapse_id) collapses.find((element) => element.id == collapse_id).collapse_status = false;
		}
		// console.log(collapses);
		this.setState({ collapse_id: id, collapses }, () => {
			// console.log(this.state.collapse_id, this.state.collapses)
		});
	};

	showHide = (id) => {
		// console.log('show/hide');
		const { collapse_id, collapses } = this.state;

		if (collapse_id == id) {
			collapses.find((element) => element.id == id).block_status = !collapses.find((element) => element.id == id)
				.block_status;
		}
		this.setState({ collapses });
	};

	onDelete = (id) => {
		const collapses = this.state.collapses.filter((element) => element.id !== id);
		this.setState({
			collapses,
			collapse_id: ''
		});
	};

	onDuplicate = (id) => {
		const { collapses } = this.state;
		let currentLength = collapses.length; // 4
		const curentObject = collapses.find((element) => element.id == id);
		const duplicateObject = JSON.parse(JSON.stringify(curentObject));
		duplicateObject.id = currentLength + 1;
		duplicateObject.collapse_status = false;
		let currentIndex = id - 1;
		collapses.splice(currentIndex, 0, duplicateObject);
		this.setState({
			collapses
		});
	};

	render() {
		return (
			<React.Fragment>
				<div className="container">
					<div className="section_block">
						{this.state.collapses.map((collapse) => (
							<Collapses
								key={collapse.id}
								id={collapse.id}
								collapse={collapse}
								propsExpandCollapse={this.expandCollapse}
								propsShowHide={this.showHide}
								propsOnDelete={this.onDelete}
								propsOnDuplicate={this.onDuplicate}
								cards={this.state.cards}
							/>
						))}
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default Project_A;
